# vue-admin-template

English | [简体中文](./README-zh.md)

## Build Setup


```bash
# clone the project
git clone https://gitee.com/chenq7/vue-backend-router-template.git

# enter the project directory
cd vue-backend-router-template

# install dependency
npm install

# develop
npm run dev
```

This will automatically open http://localhost:9528

## Build

```bash
# build for test environment
npm run build:stage

# build for production environment
npm run build:prod
```